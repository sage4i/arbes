package com.phonecompany.billing;

import com.phonecompany.billing.definition.CallLog;
import com.phonecompany.billing.definition.PhoneNumbersMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public final class TelephoneBillCalculatorImpl implements TelephoneBillCalculator {
    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    @Override
    public BigDecimal calculate(String phoneLog) {
        log.info("New telephone bill");
        String[] _phoneLog = phoneLog.split("\n");
        Objects.requireNonNull(_phoneLog);

        List<CallLog> callLogList = new LinkedList<>();
        PhoneNumbersMap phoneNumbersMap = new PhoneNumbersMap();

        for (String _callLog : _phoneLog) {
            try {
                CallLog callLog = CallLog.parse(_callLog);
                callLogList.add(callLog);
                phoneNumbersMap.add(callLog);

            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }

        String mostUsedPhoneNumber = phoneNumbersMap.getMostUsedPhoneNumber();
        log.info("Promoted number {}", mostUsedPhoneNumber);

        BigDecimal amountToPay = new BigDecimal(0);
        for (CallLog callLog : callLogList) {
            log.info(callLog.toString());
            if (mostUsedPhoneNumber.equals(callLog.getPhoneNumber()))
                continue;

            amountToPay = amountToPay.add(callLog.calculate());
        }

        return amountToPay;
    }
}
