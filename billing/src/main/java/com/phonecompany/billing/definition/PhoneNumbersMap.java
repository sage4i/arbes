package com.phonecompany.billing.definition;

import java.util.TreeMap;

public final class PhoneNumbersMap extends TreeMap<String, Long> {
    public void add(CallLog callLog) {
        long count = 0;
        if (containsKey(callLog.getPhoneNumber())) {
            count = get(callLog.getPhoneNumber());
        }
        put(callLog.getPhoneNumber(), count + 1);
    }

    public String getMostUsedPhoneNumber() {
        String mostUsedPhoneNumber = null;
        long max = Long.MIN_VALUE;

        for (String phoneNumber : keySet()) {
            if (max <= get(phoneNumber)) {
                mostUsedPhoneNumber = phoneNumber;
                max = get(phoneNumber);
            }
        }

        return mostUsedPhoneNumber;
    }
}
