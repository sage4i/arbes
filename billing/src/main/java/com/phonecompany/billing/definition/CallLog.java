package com.phonecompany.billing.definition;

import com.phonecompany.billing.io.Formatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Date;

public final class CallLog {
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final String phoneNumber;
    private final Date startTime, endTime;

    private CallLog(String phoneNumber, Date startTime, Date endTime) {
        this.phoneNumber = phoneNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static CallLog parse(String callLog) throws Exception {
        String[] _callLog = callLog.split(",");
        if (_callLog.length != 3) {
            throw new Exception("Incorrect call log format");
        }
        return new CallLog(Formatter.checkPhoneNumber(_callLog[0]),
                Formatter.parseTimestamp(_callLog[1]),
                Formatter.parseTimestamp(_callLog[2]));
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return "Call | " + phoneNumber + " | " + startTime + ' ' + endTime;
    }

    public BigDecimal calculate() {
        BigDecimal amountToPay = new BigDecimal(0);
        int fiveMinutesCheck = 0;
        for (Date minuteOfCall = (Date) startTime.clone(); minuteOfCall.before(endTime); minuteOfCall = Formatter.addMinute(minuteOfCall)) {
            double intervalRate;

            if (fiveMinutesCheck >= 5) {
                intervalRate = Rate.RATE_5_MINUTES;

            } else {
                if (Formatter.isHourInInterval(minuteOfCall, 8, 16))
                    intervalRate = Rate.RATE_INTERVAL;
                else
                    intervalRate = Rate.RATE_NON_INTERVAL;

                fiveMinutesCheck++;
            }

            log.info("Minute " + minuteOfCall + " " + intervalRate);
            amountToPay = amountToPay.add(BigDecimal.valueOf(intervalRate));
        }

        log.info("Amount to pay " + amountToPay.toPlainString());
        return amountToPay;
    }
}
