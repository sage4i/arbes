package com.phonecompany.billing.definition;

public interface Rate {
    double RATE_INTERVAL = 1;
    double RATE_NON_INTERVAL = 0.5;
    double RATE_5_MINUTES = 0.2;
}
