package com.phonecompany.billing.io;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public interface Formatter {
    SimpleDateFormat FORMAT_DMYHMS = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);

    static Date parseTimestamp(String dateTime) throws ParseException {
        return FORMAT_DMYHMS.parse(dateTime);
    }

    static String checkPhoneNumber(String phoneNumber) throws Exception {
        if (phoneNumber.matches("[0-9]+") && phoneNumber.length() == 12)
            return phoneNumber;
        else throw new Exception("Incorrect phone number format");
    }

    static Date addMinute(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, 1);
        return cal.getTime();
    }

    static boolean isHourInInterval(Date date, int start, int end) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return (start <= cal.get(Calendar.HOUR) && cal.get(Calendar.HOUR) <= end);
    }
}
