package com.phonecompany.billing.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;

public interface FileManager {
    static String getInputDirectory() {
        return System.getProperty("user.dir") + File.separator + "inputs" + File.separator;
    }

    static String parseFile(String fileName) throws IOException {
        Objects.requireNonNull(fileName);
        StringBuilder content = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(getInputDirectory() + fileName));
        while (reader.ready()) {
            content.append(reader.readLine()).append("\n");
        }
        return content.toString();
    }
}
