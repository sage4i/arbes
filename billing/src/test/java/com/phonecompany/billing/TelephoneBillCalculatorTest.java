package com.phonecompany.billing;

import com.phonecompany.billing.io.FileManager;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class TelephoneBillCalculatorTest {
    private static String content;

    @Test
    public void testParseFile() throws IOException {
        content = FileManager.parseFile("phone calls1.csv");
        assertThat(content, containsString("420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57"));
        assertThat(content, containsString("420776562353,18-01-2020 08:59:20,18-01-2020 09:10:00"));
    }

    @Test
    public void testTelephoneBillCalculator1() {
        assertEquals(new BigDecimal("1.5").setScale(1, RoundingMode.HALF_EVEN),
                new TelephoneBillCalculatorImpl().calculate(content).setScale(1, RoundingMode.HALF_EVEN));
    }

    @Test
    public void testTelephoneBillCalculator2() throws IOException {
        content = FileManager.parseFile("phone calls2.csv");
        assertEquals(new BigDecimal("6.2").setScale(1, RoundingMode.HALF_EVEN),
                new TelephoneBillCalculatorImpl().calculate(content).setScale(1, RoundingMode.HALF_EVEN));
    }

    @Test
    public void testTelephoneBillCalculator3() throws IOException {
        content = FileManager.parseFile("phone calls3.csv");
        assertEquals(new BigDecimal("582.2").setScale(1, RoundingMode.HALF_EVEN),
                new TelephoneBillCalculatorImpl().calculate(content).setScale(1, RoundingMode.HALF_EVEN));
    }
}
