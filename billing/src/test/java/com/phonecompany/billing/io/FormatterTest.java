package com.phonecompany.billing.io;

import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;

import static org.junit.Assert.*;

public class FormatterTest {
    @Test
    public void testDateTimeParser() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2020);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.set(Calendar.DAY_OF_MONTH, 13);
        calendar.set(Calendar.HOUR, 18);
        calendar.set(Calendar.MINUTE, 12);
        calendar.set(Calendar.SECOND, 57);
        calendar.set(Calendar.MILLISECOND, 0);

        assertEquals(calendar.getTime(), Formatter.parseTimestamp("13-01-2020 18:12:57"));
    }

    @Test
    public void testCheckPhoneNumber() {
        Exception exception = assertThrows(Exception.class, () -> Formatter.checkPhoneNumber("420776A62353"));

        String expectedMessage = "Incorrect phone number format";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}
